var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { AppConfigurationService } from "./app-configuration.service";
var PostalCodeValidationService = /** @class */ (function () {
    function PostalCodeValidationService(appConfiguration, httpService) {
        this.appConfiguration = appConfiguration;
        this.httpService = httpService;
    }
    PostalCodeValidationService.prototype.validate = function (postalCode) {
        var _this = this;
        return Observable.create(function (observer) {
            _this.appConfiguration.load().subscribe(function (configInfo) {
                _this.httpService
                    .get(configInfo.validatePostalCode, { params: { postalCode: postalCode } }).subscribe(function (validationResponse) {
                    observer.next(validationResponse);
                    observer.complete();
                }, function () {
                    observer.err();
                });
            });
        });
    };
    PostalCodeValidationService = __decorate([
        Injectable({
            providedIn: "root"
        }),
        __metadata("design:paramtypes", [AppConfigurationService, HttpClient])
    ], PostalCodeValidationService);
    return PostalCodeValidationService;
}());
export { PostalCodeValidationService };
//# sourceMappingURL=postal-code-validation.service.js.map