var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
//component
import { RequestConsultationFormComponent } from "./request-consultation-form/request-consultation-form.component";
import { HomeComponent } from "./home/home.component";
//route
var routes = [
    { path: "", component: HomeComponent },
    { path: "request-consultation/:concept", component: RequestConsultationFormComponent }
];
var AppRouterModule = /** @class */ (function () {
    function AppRouterModule() {
    }
    AppRouterModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRouterModule);
    return AppRouterModule;
}());
export { AppRouterModule };
//# sourceMappingURL=app-router.module.js.map