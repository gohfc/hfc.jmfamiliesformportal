import { async, TestBed } from '@angular/core/testing';
import { RequestConsultationFormComponent } from './request-consultation-form.component';
describe('RequestConsultationFormComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [RequestConsultationFormComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(RequestConsultationFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=request-consultation-form.component.spec.js.map