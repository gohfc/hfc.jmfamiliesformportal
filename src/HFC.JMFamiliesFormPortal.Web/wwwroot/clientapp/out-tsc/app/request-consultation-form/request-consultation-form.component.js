var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import { AppConfigurationService } from "../app-configuration.service";
var RequestConsultationFormComponent = /** @class */ (function () {
    function RequestConsultationFormComponent(route, httpService, appConfiguration) {
        var _this = this;
        this.route = route;
        this.httpService = httpService;
        this.appConfiguration = appConfiguration;
        this.concept = this.route.snapshot.paramMap.get("concept");
        this.postalCodeFormModel = {};
        this.requestFormModel = {};
        this.startSpinner();
        this.appConfiguration.load().subscribe(function (response) {
            _this.bootstrapInfo = response;
            _this.httpService
                .get(response.getConcept, { params: { concept: _this.concept } })
                .subscribe(function (response) {
                _this.loadedConcept = true;
                _this.conceptInfo = response;
            }, function (err) {
                alert("An error occurred");
                _this.upstreamError = true;
            });
        }, function (err) {
            alert("Couldn't load bootstrapper info");
            _this.upstreamError = true;
        });
    }
    RequestConsultationFormComponent.prototype.reset = function () {
        delete this.lastResponse;
        this.postalCodeFormModel.postalCode = null;
        this.mainForm.reset();
        this.postalCodeForm.reset();
        this.requestFormModel = {};
    };
    RequestConsultationFormComponent.prototype.validConcept = function () {
        return !!(this.conceptInfo);
    };
    RequestConsultationFormComponent.prototype.showThankYou = function () {
        return this.done && this.success;
    };
    RequestConsultationFormComponent.prototype.showWhoops = function () {
        return this.upstreamError || (this.done && (!this.success));
    };
    RequestConsultationFormComponent.prototype.showForConcept = function () {
        return !this.conceptInfo;
    };
    RequestConsultationFormComponent.prototype.showZipForm = function () {
        if (this.done) {
            return false;
        }
        if (!this.validConcept()) {
            return false;
        }
        if (!this.haveLastResponse()) {
            return true;
        }
        return !(this.lastResponse);
    };
    RequestConsultationFormComponent.prototype.showInvalidPostalCode = function () {
        if (this.done) {
            return false;
        }
        if (!this.validConcept()) {
            return false;
        }
        if (this.localPostalCodeInvalid) {
            return true;
        }
        if (!this.haveLastResponse()) {
            return false;
        }
        else {
            return this.lastResponse.invalidPostalCode;
        }
    };
    RequestConsultationFormComponent.prototype.startSpinner = function () {
        if (this.spinnerHandle) {
            window.clearTimeout(this.spinnerHandle);
        }
        this.showSpinner = true;
        this.spinnerHandle = window.setTimeout((function (ref) { return function () { ref.showSpinner = false; }; })(this), 1000);
    };
    RequestConsultationFormComponent.prototype.showInvalidConcept = function () {
        if (!this.loadedConcept) {
            return false;
        }
        if (this.done) {
            return false;
        }
        if (!this.bootstrapInfo) {
            return false;
        }
        return !this.validConcept();
    };
    RequestConsultationFormComponent.prototype.haveLastResponse = function () {
        if (typeof this.lastResponse === "undefined") {
            return false;
        }
        else if (this.lastResponse == null) {
            return false;
        }
        else {
            return true;
        }
    };
    RequestConsultationFormComponent.prototype.currentDirName = function () {
        return this.haveLastResponse() ? this.lastResponse.dirName : null;
    };
    RequestConsultationFormComponent.prototype.showNoMatch = function () {
        if (this.done) {
            return false;
        }
        if (!this.validConcept() || typeof this.lastResponse === "undefined") {
            return false;
        }
        else {
            return this.lastResponse == null;
        }
    };
    RequestConsultationFormComponent.prototype.getLastResponseJson = function () {
        return JSON.stringify(this.lastResponse || null);
    };
    RequestConsultationFormComponent.prototype.showForm = function () {
        if (this.done) {
            return false;
        }
        return this.haveLastResponse();
    };
    RequestConsultationFormComponent.prototype.onSubmit = function () {
        var _this = this;
        var toSubmit = {
            ZipCode: this.postalCodeFormModel.postalCode,
            Email: this.requestFormModel.email,
            Name: this.requestFormModel.name,
            Phone: this.requestFormModel.phone,
            TerrCode: this.lastResponse.territoryCode,
            RequestUrl: window.location.href,
            Referrer: document.referrer,
            Site: "US",
            Franchise: this.conceptInfo.zipLookupBrandType,
            ContactTypeId: [1]
        };
        this.startSpinner();
        this.requestFormBusy = true;
        window.setTimeout((function (outerThis) { return function () {
            outerThis.appConfiguration.load().subscribe(function (configInfo) {
                outerThis.httpService
                    .post(configInfo.postLead, { request: toSubmit, type: outerThis.conceptInfo.type })
                    .subscribe(function (response) {
                    outerThis.requestFormBusy = false;
                    outerThis.done = true;
                    _this.success = response;
                }, function (err) {
                    outerThis.requestFormBusy = false;
                    alert("An error occurred");
                    _this.upstreamError = true;
                });
            });
        }; })(this), 500);
    };
    RequestConsultationFormComponent.prototype.validPostalCode = function (postalCode) {
        return this.httpService
            .get(this.bootstrapInfo.validatePostalCode, { params: { postalCode: postalCode } });
    };
    RequestConsultationFormComponent.prototype.onSubmitPostalCodeForm = function () {
        var _this = this;
        this.postalCodeFormBusy = true;
        window.setTimeout((function (outerThis) { return function () {
            outerThis.appConfiguration.load().subscribe(function (configInfo) {
                outerThis.httpService
                    .post(configInfo.getTerritory, { postalCode: outerThis.postalCodeFormModel.postalCode, brandType: outerThis.conceptInfo.zipLookupBrandType })
                    .subscribe(function (response) {
                    outerThis.postalCodeFormBusy = false;
                    outerThis.lastResponse = response;
                }, function (err) {
                    outerThis.postalCodeFormBusy = false;
                    _this.upstreamError = true;
                });
            });
        }; })(this), 500);
    };
    RequestConsultationFormComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        ViewChild('mainForm'),
        __metadata("design:type", NgForm)
    ], RequestConsultationFormComponent.prototype, "mainForm", void 0);
    __decorate([
        ViewChild('postalCodeForm'),
        __metadata("design:type", NgForm)
    ], RequestConsultationFormComponent.prototype, "postalCodeForm", void 0);
    RequestConsultationFormComponent = __decorate([
        Component({
            selector: "app-request-consultation-form",
            templateUrl: "./request-consultation-form.component.html",
            styleUrls: ["./request-consultation-form.component.css"]
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            HttpClient,
            AppConfigurationService])
    ], RequestConsultationFormComponent);
    return RequestConsultationFormComponent;
}());
export { RequestConsultationFormComponent };
//# sourceMappingURL=request-consultation-form.component.js.map