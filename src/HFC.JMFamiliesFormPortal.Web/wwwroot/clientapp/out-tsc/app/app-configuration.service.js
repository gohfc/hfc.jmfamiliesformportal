var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
var AppConfigurationService = /** @class */ (function () {
    function AppConfigurationService(httpService) {
        this.httpService = httpService;
    }
    AppConfigurationService.prototype.load = function () {
        var _this = this;
        if (this.configInfo) {
            return of(this.configInfo);
        }
        return Observable.create(function (observer) {
            _this.httpService.get("/api/formbootstrapper")
                .subscribe(function (response) {
                _this.configInfo = response;
                observer.next(response);
                observer.complete();
            }, function (err) {
                observer.err();
                //alert("Couldn't load bootstrapper info");
            });
        });
    };
    AppConfigurationService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], AppConfigurationService);
    return AppConfigurationService;
}());
export { AppConfigurationService };
//# sourceMappingURL=app-configuration.service.js.map