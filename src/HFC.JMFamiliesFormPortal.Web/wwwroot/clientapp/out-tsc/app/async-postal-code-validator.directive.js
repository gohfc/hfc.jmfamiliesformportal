var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Directive } from "@angular/core";
import { NG_ASYNC_VALIDATORS } from "@angular/forms";
import { Observable } from "rxjs";
import { timer } from "rxjs";
import { PostalCodeValidationService } from "./postal-code-validation.service";
var AsyncPostalCodeValidatorDirective = /** @class */ (function () {
    function AsyncPostalCodeValidatorDirective(validationService) {
        this.validationService = validationService;
    }
    AsyncPostalCodeValidatorDirective_1 = AsyncPostalCodeValidatorDirective;
    AsyncPostalCodeValidatorDirective.prototype.validate = function (control) {
        var _this = this;
        return Observable.create(function (observer) {
            //wait 1 second before sending the request
            timer(500).subscribe(function () {
                _this.validationService.validate(control.value).subscribe(function (validationResponse) {
                    observer.next(validationResponse ? null : { invalidPostalCode: true });
                    observer.complete();
                }, function () {
                    observer.err();
                });
            });
        });
    };
    var AsyncPostalCodeValidatorDirective_1;
    AsyncPostalCodeValidatorDirective = AsyncPostalCodeValidatorDirective_1 = __decorate([
        Directive({
            selector: "[appAsyncPostalCodeValidator]",
            providers: [
                {
                    provide: NG_ASYNC_VALIDATORS,
                    useExisting: AsyncPostalCodeValidatorDirective_1,
                    multi: true
                }
            ]
        }),
        __metadata("design:paramtypes", [PostalCodeValidationService])
    ], AsyncPostalCodeValidatorDirective);
    return AsyncPostalCodeValidatorDirective;
}());
export { AsyncPostalCodeValidatorDirective };
//# sourceMappingURL=async-postal-code-validator.directive.js.map