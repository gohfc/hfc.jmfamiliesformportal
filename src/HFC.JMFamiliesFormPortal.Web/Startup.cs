﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace HFC.JMFamiliesFormPortal.Web
{
    public class Startup
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnvironmentContainer.HostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(opt =>
                    opt.SerializerSettings.ReferenceLoopHandling =
                        ReferenceLoopHandling.Ignore) //ignores self reference object 
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1); //validate api rules

            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "wwwroot/clientapp/dist"; });

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
        }

        private IApplicationBuilder UseLog4NetForUnhandledExceptions(IApplicationBuilder applicationBuilder1)
        {
            return applicationBuilder1.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    await Task.Run(() =>
                    {
                        var exceptionHandlerPathFeature =
                            context.Features.Get<IExceptionHandlerPathFeature>();
                        var ex = exceptionHandlerPathFeature?.Error;
                        if (ex != null)
                            log.Error("Unhandled exception", ex);
                    });
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                UseLog4NetForUnhandledExceptions(app.UseDeveloperExceptionPage());
            else
                UseLog4NetForUnhandledExceptions(app);

            AppDomain.CurrentDomain.SetData("AppData", Path.Combine(env.ContentRootPath, "app_data"));


            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.Use(async (context, next) =>
            {
                LogHelper.InitializeRequest(new Uri(context.Request.GetDisplayUrl()), env.ApplicationName);
                await next.Invoke();
            });

            app.UseMvc();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment()) spa.UseAngularCliServer("start");
            });
        }
    }
}