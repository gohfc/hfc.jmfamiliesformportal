﻿namespace HFC.JMFamiliesFormPortal.Web
{
    public static class RouteNames
    {
        public class Generic
        {
            public const string IsValidPostalCode = "ee2fae5c-1f6f-4b89-be65-f0cf6152ab6e";
            public const string GetTerritoryInfo = "de79254e-a5f4-45b2-8a6f-9bda9ec01c5e";
        }

        public class Concept
        {
            public const string Get = "f23a1442-d327-46cd-bf4e-d37566a9c2a2";
            public const string GetAll = "e5cf04a4-231f-4c28-bbd0-2ff8d4fc4cc8";
        }

        public class LeadController
        {
            public const string Post = "54f4368b-7006-4f3c-b376-8986528d3c93";
        }
    }
}