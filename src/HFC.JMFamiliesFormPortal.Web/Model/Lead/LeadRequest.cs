﻿using HFC.JMFamiliesFormPortal.Web.Model.Generic;
using Newtonsoft.Json;

namespace HFC.JMFamiliesFormPortal.Web.Model.Lead
{
    /*
     {
    "ErrorRequired": "",
    "ErrorGeneral": "",
    "ErrorRedirect": "",
    "RedirectDestination": "/request-your-free-consultation/thank-you-ihc-match/",
    "zipErrorRedirectDestination": "/request-your-free-consultation/thank-you-ihc-nomatch/",
    "Name": "test",
    "Email": "k65dohzsxs01@opayq.com",
    "FirstName": "test",
    "LastName": "",
    "Phone": "1111111111",
    "ZipCode": "88888",
    "ContactTypeAllSelected": true,
    "ContactTypeId": [1],
    "MetaData": "eyJ3ZWJfdXNlcmlkIjoiMTg3MDk2MDc2ODY0NDYzMSIsInNpdGVfdHJhY2tpbmdfaWQiOiI5NTY4ZDg5MS1kZTNhLTRjZDAtYjQwMS0xNTJiYTIwZDcwMWUiLCJyZXF1ZXN0X2lwIjoiOTguMTkxLjE1MS4yMTgiLCJ0aW1lc3RhbXAiOjE1NjkzNTU0NDE1MTUsInJlcXVlc3RfaG9zdCI6ImxvY2FsaG9zdCIsImxlYWRfZm9ybV91cmwiOiJodHRwOi8vbG9jYWxob3N0OjU4MzY4LyJ9",
    "FirstVisitDate": "2019-09-06T16:02:02",
    "Campaign": "cake1",
    "TerrCode": "0888",
    "RequestType": "InHome",
    "RequestUrl": "http://localhost:58368/",
    "Referrer": "http://localhost:58368/",
    "IP": "172.16.21.146",
    "Site": "US"
}
     */
    public class LeadRequest
    {
        private string _name;
        [JsonIgnore] public PostalCodeInfo PostalCodeInfo { get; private set; } = PostalCodeInfo.Empty;

        [JsonProperty("zipCode")]
        public virtual string ZipCode
        {
            get => PostalCodeInfo?.Value;
            set => PostalCodeInfo = PostalCodeInfo.Parse(value);
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                LastName = null;
                FirstName = null;
                if (!string.IsNullOrEmpty(value))
                {
                    var idx = value.IndexOf(' ');
                    if (idx >= 0)
                    {
                        FirstName = value.Substring(0, idx);
                        LastName = value.Substring(idx + 1);
                    }
                }
            }
        }

        public string Email { get; set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Phone { get; set; }

       
        public int[] ContactTypeId { get; set; } = new[] {1};

        public string Franchise { get; set; }
        //public DateTime FirstVisitDate { get; set; }
        public string Campaign { get; set; }
        public string TerrCode { get; set; }
        public string RequestType { get; set; }
        public string RequestUrl { get; set; }
        public string Referrer { get; set; }
        public string IP { get; set; }
        public string Site { get; set; } = "US";
    }
}