﻿namespace HFC.JMFamiliesFormPortal.Web.Model.Lead
{
    public class LeadRequestWrapper
    {
        public string Type { get; set; }
        public LeadRequest Request { get; set; }
    }
}