﻿using Newtonsoft.Json;

namespace HFC.JMFamiliesFormPortal.Web.Model.Concept
{
    public class ConceptInfo
    {
        [JsonProperty("tcpaText")] public string TcpaText { get; set; }
        [JsonProperty("imagePath")] public string ImagePath { get; set; }
        [JsonProperty("imageBackground")] public string ImageBackground { get; set; }
        [JsonProperty("type")] public string Type { get; set; }
        [JsonProperty("zipLookupBrandType")] public string ZipLookupBrandType { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("enabled")] public bool Enabled { get; set; }
        [JsonProperty("privacyPolicyUrl")] public string PrivacyPolicyUrl { get; set; }
        [JsonProperty("insertText")] public string InsertText { get; set; }
    }
}