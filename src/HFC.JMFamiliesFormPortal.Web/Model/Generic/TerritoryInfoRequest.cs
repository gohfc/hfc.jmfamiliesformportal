﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace HFC.JMFamiliesFormPortal.Web.Model.Generic
{
    public class TerritoryInfoRequest:IValidatableObject
    {
        [JsonProperty("postalCode")]
        public virtual string PostalCode
        {
            get => PostalCodeInfo?.Value;
            set => PostalCodeInfo = PostalCodeInfo.Parse(value);
        }
        [JsonProperty("brandType")]
        public virtual string BrandType { get; set; }

        [JsonIgnore] public PostalCodeInfo PostalCodeInfo { get; private set; } = PostalCodeInfo.Empty;

        public bool IsValid()
        {
            return PostalCodeInfo.IsValidUsOrCanada;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!PostalCodeInfo.IsValidUsOrCanada)
            {
                yield return  new ValidationResult("Please enter a valid US or Canada postal code.");
            }
        }
    }
}