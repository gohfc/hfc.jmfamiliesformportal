﻿namespace HFC.JMFamiliesFormPortal.Web.Model.Generic
{
    public enum PostalCodeTypeEnum
    {
        Unknown,
        UnitedStates,
        Canada
    }
}