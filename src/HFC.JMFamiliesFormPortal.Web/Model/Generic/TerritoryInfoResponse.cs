﻿namespace HFC.JMFamiliesFormPortal.Web.Model.Generic
{
    public class TerritoryInfoResponse
    {
        public string ID { get; set; }
        public string TerritoryCode { get; set; }
        public object Name { get; set; }
        public object ManagerFName { get; set; }
        public object ManagerLName { get; set; }
        public object Email { get; set; }
        public object Phone { get; set; }
        public string DirName { get; set; }
        public bool IsMyCRM { get; set; }
        public bool IsTPT { get; set; }
        public object OwnerNumber { get; set; }
        public object Contract { get; set; }
        public object POName { get; set; }
        public object ZipCode { get; set; }
        public string TerritoryName { get; set; }
    }
}