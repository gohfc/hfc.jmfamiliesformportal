﻿using System;
using System.Text.RegularExpressions;

namespace HFC.JMFamiliesFormPortal.Web.Model.Generic
{
    public sealed class PostalCodeInfo
    {
        private static PostalCodeInfo _empty;
        private static readonly object Mutex = new object();

        private static readonly Regex UsZipRegex = new Regex(@"^\d{5}[-\s]?(?:\d{4})?$", RegexOptions.Compiled);

        private static readonly Regex CanadaZipRegex =
            new Regex(@"^[A-Za-z]\d[A-Za-z][ -]?(?:\d[A-Za-z]\d)?$", RegexOptions.Compiled);

        private static readonly PostalCodeTypeEnum[] USAndCanadaPostalCodeTypes =
            {PostalCodeTypeEnum.UnitedStates, PostalCodeTypeEnum.Canada};

        internal PostalCodeInfo(string postalCode, PostalCodeTypeEnum type)
        {
            Type = type;
            Value = postalCode?.ToUpper();
            IsValidUsOrCanada = Array.IndexOf(USAndCanadaPostalCodeTypes, type) >= 0;
        }

        public static PostalCodeInfo Empty
        {
            get
            {
                lock (Mutex)
                {
                    return _empty ?? (_empty = new PostalCodeInfo(null, PostalCodeTypeEnum.Unknown));
                }
            }
        }

        public bool IsValidUsOrCanada { get; }

        public string Value { get; }
        public PostalCodeTypeEnum Type { get; }


        /// <summary>
        ///     this normalizes us zip codes to 5 digits with only numeric characters, if valid, or to canadian to 3/6 with only
        ///     alphanumeric characters.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static PostalCodeInfo Parse(string value)
        {
            PostalCodeTypeEnum type;
            if (string.IsNullOrWhiteSpace(value)) return Empty;

            if (CanadaZipRegex.IsMatch(value))
            {
                type = PostalCodeTypeEnum.Canada;
            }
            else
            {
                if (UsZipRegex.IsMatch(value))
                    type = PostalCodeTypeEnum.UnitedStates;
                else
                    type = PostalCodeTypeEnum.Unknown;
            }


            switch (type)
            {
                case PostalCodeTypeEnum.UnitedStates:
                    return new PostalCodeInfo(value.Substring(0, 5), type);
                case PostalCodeTypeEnum.Canada:
                    return new PostalCodeInfo(Regex.Replace(value, "[- ]", string.Empty), type);
                default:
                    return new PostalCodeInfo(value, type);
            }
        }
    }
}