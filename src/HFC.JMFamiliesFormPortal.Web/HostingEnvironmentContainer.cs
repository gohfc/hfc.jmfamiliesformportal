﻿using Microsoft.AspNetCore.Hosting;

namespace HFC.JMFamiliesFormPortal.Web
{
    public class HostingEnvironmentContainer
    {
        public static IHostingEnvironment HostingEnvironment { get; set; }
    }
}