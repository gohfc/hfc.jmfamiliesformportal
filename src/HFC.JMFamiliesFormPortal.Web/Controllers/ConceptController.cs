﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HFC.JMFamiliesFormPortal.Web.Model.Concept;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HFC.JMFamiliesFormPortal.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConceptController : Controller
    {
        // GET api/values
        [HttpGet]
        [Route("concept", Name = RouteNames.Concept.Get)]
        public ConceptInfo Get(string concept)
        {
            var concepts = GetConceptDictionary();
            if (concepts.ContainsKey(concept) && concepts[concept].Enabled) return concepts[concept];

            return null;
        }

        private Dictionary<string, ConceptInfo> GetConceptDictionary()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.GetData("AppData").ToString(), "concept.json");
            var concepts = new Dictionary<string, ConceptInfo>(
                JsonConvert.DeserializeObject<Dictionary<string, ConceptInfo>>(System.IO.File.ReadAllText(path)),
                StringComparer.InvariantCultureIgnoreCase);
            foreach (var result in concepts.Values)
                if (!string.IsNullOrEmpty(result.ImagePath))
                    result.ImagePath = Url.Content(result.ImagePath);
            return concepts;
        }

        // GET api/values
        [HttpGet]
        [Route("concepts", Name = RouteNames.Concept.GetAll)]
        public List<ConceptInfo> GetAll()
        {
            var concepts = GetConceptDictionary();
            return concepts.Values.Where(i => i.Enabled).OrderBy(i => i.Name).ToList();
        }
    }
}