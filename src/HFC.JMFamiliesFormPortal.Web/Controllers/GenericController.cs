﻿using System;
using System.Reflection;
using HFC.JMFamiliesFormPortal.Web.Model.Generic;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HFC.JMFamiliesFormPortal.Web.Controllers
{
    [Route("api/[controller]")]
    public class GenericController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Uri _getTerritoryInfoUri;

        public GenericController(IOptions<AppSettings> settings)
        {
            _getTerritoryInfoUri = new UriBuilder(new Uri(settings.Value.HFCServicesLeadUrl))
            {
                Query = "", Path = "/api/territory/infobypostalcode", Fragment = ""
            }.Uri;
        }


        [HttpPost]
        [Route("getTerritoryInfoByPostalCode", Name = RouteNames.Generic.GetTerritoryInfo)]
        public object GetTerritoryInfo([FromBody] TerritoryInfoRequest territoryInfoRequest)
        {
            if (ModelState.IsValid)

            {
                var client = new RestClient(_getTerritoryInfoUri);
                //clean up the zip code
                var postalCodeInfo = territoryInfoRequest.PostalCodeInfo;

                var restRequest = new RestRequest(Method.GET)
                    .AddParameter("territoryinfotype", "Territory")
                    .AddParameter("brandtype", territoryInfoRequest.BrandType)
                    .AddParameter("criteria", postalCodeInfo.Value);
                SiteNameHelper.AddSiteName(restRequest);
                if (log.IsInfoEnabled)
                    log.InfoFormat("Sending territory info request {0} to web services",
                        JsonConvert.SerializeObject(territoryInfoRequest));

                // or automatically deserialize result
                // return content type is sniffed but can be explicitly set via RestClient.AddHandler();
                var response = client.Execute<TerritoryInfoResponse>(restRequest);
                if (response.Data == null)
                {
                    log.Info("Received null response");
                }
                else
                {
                    if (log.IsInfoEnabled)
                        log.InfoFormat("Received response {0}", JsonConvert.SerializeObject(response.Data));
                }

                if (!string.IsNullOrEmpty(response.Data?.DirName))
                    return response.Data;
                return new {noMatchingTerritory = true};
            }

            return new {invalidRequest = true};
        }

        [HttpGet]
        [Route("isValidPostalCode", Name = RouteNames.Generic.IsValidPostalCode)]
        public bool ValidPostalCode(string postalCode)
        {
            return PostalCodeInfo.Parse(postalCode).IsValidUsOrCanada;
        }
    }
}