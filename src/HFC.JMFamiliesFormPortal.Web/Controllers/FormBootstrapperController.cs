﻿using Microsoft.AspNetCore.Mvc;

namespace HFC.JMFamiliesFormPortal.Web.Controllers
{
    [Route("api/[controller]")]
    public class FormBootstrapperController : Controller
    {
        [HttpGet]
        public object Get()
        {
            return new
            {
                getConcept = Url.RouteUrl(RouteNames.Concept.Get),
                getConcepts = Url.RouteUrl(RouteNames.Concept.GetAll),
                validatePostalCode = Url.RouteUrl(RouteNames.Generic.IsValidPostalCode),
                getTerritory = Url.RouteUrl(RouteNames.Generic.GetTerritoryInfo),
                postLead = Url.RouteUrl(RouteNames.LeadController.Post)
            };
        }
    }
}