using HFC.JMFamiliesFormPortal.Web;
using RestSharp;

internal static class SiteNameHelper
{
    public static void AddSiteName(IRestRequest restRequest)
    {
        var siteName = HostingEnvironmentContainer.HostingEnvironment?.ApplicationName;
        if (!string.IsNullOrEmpty(siteName))
            restRequest.AddHeader("X-HFC-SiteName",
                HostingEnvironmentContainer.HostingEnvironment?.ApplicationName);
    }
}