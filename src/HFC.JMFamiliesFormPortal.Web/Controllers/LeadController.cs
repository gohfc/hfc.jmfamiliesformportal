﻿using System;
using System.Net;
using System.Reflection;
using HFC.JMFamiliesFormPortal.Web.Model.Lead;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;

namespace HFC.JMFamiliesFormPortal.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeadController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly Uri _leadUrl;

        private readonly IOptions<AppSettings> _settings;

        public LeadController(IOptions<AppSettings> settings)
        {
            _settings = settings;
            _leadUrl = new UriBuilder(new Uri(settings.Value.HFCServicesLeadUrl))
            {
                Query = "", Path = "/api/generalrequest", Fragment = ""
            }.Uri;
        }


        [HttpPost]
        [Route("", Name = RouteNames.LeadController.Post)]
        public bool Post([FromBody] LeadRequestWrapper requestWrapper)
        {
            try
            {
                var leadRequest = requestWrapper.Request;

                leadRequest.Campaign = _settings.Value.Campaign;
                leadRequest.IP = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                leadRequest.RequestType = "InHome";
                leadRequest.RequestUrl = Request.GetTypedHeaders().Referer.ToString();
                if (log.IsInfoEnabled)
                    log.InfoFormat("Posting to web service {0}", JsonConvert.SerializeObject(leadRequest));
                var client = new RestClient(_leadUrl);

                var restRequest = new RestRequest(Method.POST).AddJsonBody(leadRequest);
                SiteNameHelper.AddSiteName(restRequest);
                var response = client.Execute(restRequest);
                if (string.IsNullOrEmpty(response.Content))
                    log.InfoFormat("Received empty response");
                else
                    log.InfoFormat("Received response {0}", response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}