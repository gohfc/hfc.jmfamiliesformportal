﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Gelf4Net.Appender;
using Gelf4Net.Layout;
using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using Microsoft.AspNetCore.Hosting;

namespace HFC.JMFamiliesFormPortal.Web
{
    public static class LogHelper
    {
        private const string Log4NetUrlCustomPropertyName = "url";
        private const string Log4NetHostCustomPropertyName = "host";

        private static bool Configured;

        private static readonly FileSystemWatcher _fileSystemWatcher = new FileSystemWatcher();

        public static void InitializeRequest(Uri url, string siteName)
        {
            ThreadContext.Properties["Url"] =
                url.AbsoluteUri;
            ThreadContext.Properties["Host"] =
                url.Host;
            ThreadContext.Properties["SiteName"] = HostingEnvironmentContainer.HostingEnvironment.ApplicationName;
            ThreadContext.Properties["ThreadContextId"] = Guid.NewGuid().ToString();
        }

        public static IWebHostBuilder ConfigureLog4Net(
            this IWebHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureLogging((context, builder) =>
            {
                if (Configured) return;

                Configured = true;
                var loggerRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                var configFileInfo =
                    new FileInfo(Path.Combine(context.HostingEnvironment.WebRootPath, "log4net.config"));
                //set up filewatcher to reconfigure gelf appender if "log4net.config" changes.
                _fileSystemWatcher.Path = configFileInfo.DirectoryName;
                _fileSystemWatcher.Filter = configFileInfo.Name;
                _fileSystemWatcher.Changed += delegate { ConfigureGelfAppender(true); };
                _fileSystemWatcher.EnableRaisingEvents = true;

                XmlConfigurator.Configure(loggerRepository, configFileInfo);

                ConfigureGelfAppender();
            });
        }

        private static void ConfigureGelfAppender(bool withWait = false)
        {
            if (withWait)
                //wait two seconds.  log4net will also look at file update within 500 ms and this should fire afterward
                Thread.Sleep(2000);

            var loggerRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            var hierarchy = (Hierarchy) loggerRepository;
            foreach (var appender in hierarchy.Root.Appenders.OfType<AsyncGelfUdpAppender>())

            {
                var layout = appender.Layout as GelfLayout;
                if (layout != null)
                {
                    var version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
                    layout.AdditionalFields =
                        string.Format(
                            "Version:{0},Level:%level,Method:%M,Host:%aspnet-request{{HTTP_HOST}},UserHostAddress:%aspnet-request{{REMOTE_ADDR}},UserHostName:%aspnet-request{{REMOTE_HOST}}",
                            version);
                }

                appender.ActivateOptions();
            }
        }
    }
}