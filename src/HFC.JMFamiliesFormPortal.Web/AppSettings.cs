﻿namespace HFC.JMFamiliesFormPortal.Web
{
    public class AppSettings
    {
        public string HFCServicesLeadUrl { get; set; }

        public string Campaign { get; set; }
    }
}