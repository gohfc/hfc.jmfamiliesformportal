import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import { AppConfigurationService } from "../app-configuration.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    concepts: any;

    constructor(private route: ActivatedRoute,
        private httpService: HttpClient,
        private appConfiguration: AppConfigurationService) {
        this.appConfiguration.load().subscribe(response => {
               

                this.httpService
                    .get(response.getConcepts, {   })
                    .subscribe(response => {
                            
                            this.concepts = response;
                        },
                        err => {
                            alert("An error occurred");
                        });

            },
            err => {
                alert("Couldn't load bootstrapper info");
            });


    }

  ngOnInit() {
  }

}
