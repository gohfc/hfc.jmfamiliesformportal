import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { AppConfigurationService } from "./app-configuration.service";

@Injectable({
    providedIn: "root"
})
export class PostalCodeValidationService {

    constructor(private appConfiguration: AppConfigurationService, private httpService: HttpClient) {}


    public validate(postalCode: string): Observable<boolean> {
        return Observable.create(observer => {
            this.appConfiguration.load().subscribe(configInfo => {
                this.httpService
                    .get(configInfo.validatePostalCode, { params: { postalCode: postalCode } }).subscribe(
                        (validationResponse: boolean) => {
                            observer.next(validationResponse);
                            observer.complete();
                        },
                        () => {
                            observer.err();
                        });
            });
        });

    }
}