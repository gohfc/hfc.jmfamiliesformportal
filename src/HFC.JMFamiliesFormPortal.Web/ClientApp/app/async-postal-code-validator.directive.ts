import { Directive } from "@angular/core";
import { NG_ASYNC_VALIDATORS, AbstractControl, AsyncValidator } from "@angular/forms";
import { Observable } from "rxjs";
import { timer } from "rxjs";
import { PostalCodeValidationService } from "./postal-code-validation.service";

@Directive({
    selector: "[appAsyncPostalCodeValidator]",
    providers: [
        {
            provide: NG_ASYNC_VALIDATORS,
            useExisting: AsyncPostalCodeValidatorDirective,
            multi: true

        }
    ]
})
export class AsyncPostalCodeValidatorDirective implements AsyncValidator {
    validate(control: AbstractControl): Promise<{ [index: string]: any; }> | Observable<{ [index: string]: any; }> {

        return Observable.create(observer => {
            //wait 1 second before sending the request
            timer(500).subscribe(() => {
                this.validationService.validate(control.value).subscribe(
                    (validationResponse: boolean) => {
                        observer.next(validationResponse ? null : { invalidPostalCode: true });
                        observer.complete();
                    },
                    () => {
                        observer.err();
                    });
            });

        });
    }

    constructor(
        private validationService: PostalCodeValidationService) {
    }
}