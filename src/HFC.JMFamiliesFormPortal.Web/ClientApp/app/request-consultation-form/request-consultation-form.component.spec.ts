import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestConsultationFormComponent } from './request-consultation-form.component';

describe('RequestConsultationFormComponent', () => {
  let component: RequestConsultationFormComponent;
  let fixture: ComponentFixture<RequestConsultationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestConsultationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestConsultationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
