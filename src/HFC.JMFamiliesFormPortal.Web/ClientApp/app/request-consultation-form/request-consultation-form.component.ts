import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import { AppConfigurationService } from "../app-configuration.service";

@Component({
    selector: "app-request-consultation-form",
    templateUrl: "./request-consultation-form.component.html",
    styleUrls: ["./request-consultation-form.component.css"]
})
export class RequestConsultationFormComponent implements OnInit {
    concept: string;

    mode: 0;
    lastResponse: any;
    conceptInfo: any;
    spinnerHandle: number;
    showSpinner: boolean;
    localPostalCodeInvalid: boolean;
    requestFormModel: any;
    postalCodeFormModel: any;
    bootstrapInfo: any;
    done: boolean;
    success: boolean;
    loadedConcept: boolean;
    @ViewChild('mainForm') mainForm: NgForm;
    @ViewChild('postalCodeForm') postalCodeForm: NgForm;
    postalCodeFormBusy: boolean;
    requestFormBusy: boolean;
    upstreamError: boolean;

    reset() {
        delete this.lastResponse;
        this.postalCodeFormModel.postalCode = null;
        this.mainForm.reset();
        this.postalCodeForm.reset();
        this.requestFormModel = {};
    }

    validConcept() {
        return !!(this.conceptInfo);
    }

    showThankYou() {
        return this.done && this.success;
    }

    showWhoops() {
        return this.upstreamError || (this.done && (!this.success));
    }

    showForConcept() {
        return !this.conceptInfo;
    }

    showZipForm() {
        if (this.done) {
            return false;
        }
        if (!this.validConcept()) {
            return false;
        }
        if (!this.haveLastResponse()) {
            return true;
        }
        return !(this.lastResponse);
    }

    showInvalidPostalCode() {
        if (this.done) {
            return false;
        }
        if (!this.validConcept()) {
            return false;
        }
        if (this.localPostalCodeInvalid) {
            return true;
        }
        if (!this.haveLastResponse()) {
            return false;
        } else {
            return this.lastResponse.invalidPostalCode;
        }
    }

    startSpinner() {
        if (this.spinnerHandle) {
            window.clearTimeout(this.spinnerHandle);
        }

        this.showSpinner = true;
        this.spinnerHandle = window.setTimeout((ref => () => { ref.showSpinner = false; })(this),
            1000);
    }

    showInvalidConcept() {
        if (!this.loadedConcept) {
            return false;
        }
        if (this.done) {
            return false;
        }
        if (!this.bootstrapInfo) {
            return false;
        }
        return !this.validConcept();
    }

    haveLastResponse() {
        if (typeof this.lastResponse === "undefined") {
            return false;
        } else if (this.lastResponse == null) {
            return false;
        } else {
            return true;
        }
    }

    currentDirName() {
        return this.haveLastResponse() ? this.lastResponse.dirName : null;
    }

    showNoMatch() {
        if (this.done) {
            return false;
        }
        if (!this.validConcept() || typeof this.lastResponse === "undefined") {
            return false;
        } else {
            return this.lastResponse == null;
        }
    }
    getLastResponseJson() {
        return JSON.stringify(this.lastResponse || null);
    }
    showForm() {
        if (this.done) {
            return false;
        }
        return this.haveLastResponse();
    }

    onSubmit() {
        const toSubmit = {
             
            ZipCode: this.postalCodeFormModel.postalCode,
            Email: this.requestFormModel.email,
            Name: this.requestFormModel.name,
            Phone: this.requestFormModel.phone,
            TerrCode: this.lastResponse.territoryCode,
            RequestUrl: window.location.href,
            Referrer: document.referrer,
            Site: "US",
            Franchise: this.conceptInfo.zipLookupBrandType,
            ContactTypeId: [1]
        };

        this.startSpinner();
        this.requestFormBusy = true;
        window.setTimeout(
            (outerThis => () => {
                outerThis.appConfiguration.load().subscribe(configInfo => {
                    outerThis.httpService
                        .post(configInfo.postLead, { request: toSubmit, type: outerThis.conceptInfo.type })
                        .subscribe((response: boolean) => {
                            outerThis.requestFormBusy = false;
                            outerThis.done = true;
                            this.success = response;
                        },
                            err => {
                                outerThis.requestFormBusy = false;
                                alert("An error occurred");
                                this.upstreamError = true;
                            });
                });

            })(this),
            500);
    }

    constructor(private route: ActivatedRoute,
        private httpService: HttpClient,
        private appConfiguration: AppConfigurationService) {
        this.concept = this.route.snapshot.paramMap.get("concept");
        this.postalCodeFormModel = {};
        this.requestFormModel = {};
        this.startSpinner();
        this.appConfiguration.load().subscribe(response => {
            this.bootstrapInfo = response;

            this.httpService
                .get(response.getConcept, { params: { concept: this.concept } })
                .subscribe(response => {
                    this.loadedConcept = true;
                    this.conceptInfo = response;
                },
                    err => {
                        alert("An error occurred");
                        this.upstreamError = true;
                    });

        },
            err => {
                alert("Couldn't load bootstrapper info");
                this.upstreamError = true;
            });


    }

    validPostalCode(postalCode: string) {

        return this.httpService
            .get(this.bootstrapInfo.validatePostalCode, { params: { postalCode: postalCode } });
    }


    onSubmitPostalCodeForm() {
        this.postalCodeFormBusy = true;

        window.setTimeout(
            (outerThis => () => {
                outerThis.appConfiguration.load().subscribe(configInfo => {
                    outerThis.httpService
                        .post(configInfo.getTerritory, { postalCode: outerThis.postalCodeFormModel.postalCode, brandType: outerThis.conceptInfo.zipLookupBrandType })
                        .subscribe(response => {
                            outerThis.postalCodeFormBusy = false;
                            outerThis.lastResponse = response;

                        },
                            err => {
                                outerThis.postalCodeFormBusy = false;
                                this.upstreamError = true;
                            });
                });
            })(this),
            500);
    }

    ngOnInit() {
    }

}