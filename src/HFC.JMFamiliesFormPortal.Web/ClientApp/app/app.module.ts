import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { RequestConsultationFormComponent } from "./request-consultation-form/request-consultation-form.component";
import { HomeComponent } from "./home/home.component";
import { AppRouterModule } from "./app-router.module";
import { HttpClientModule } from "@angular/common/http";
import { AsyncPostalCodeValidatorDirective } from "./async-postal-code-validator.directive";

@NgModule({
    declarations: [
        AppComponent,
        FooterComponent,
        HeaderComponent,
        RequestConsultationFormComponent,
        HomeComponent,
        AsyncPostalCodeValidatorDirective
    ],
    imports: [
        BrowserModule, AppRouterModule, FormsModule, HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}