﻿import { RouterModule, Routes } from "@angular/router";
import { NgModule } from '@angular/core';  

//component
import { RequestConsultationFormComponent } from "./request-consultation-form/request-consultation-form.component";
import { HomeComponent } from "./home/home.component";

//route
const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "request-consultation/:concept", component: RequestConsultationFormComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRouterModule {
}