import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AppConfigurationService {
    configInfo: any;
    constructor(private httpService: HttpClient) { }


    load(): Observable<any> {
        if (this.configInfo) {
            return of(this.configInfo);
        }
        return Observable.create(observer => {
            this.httpService.get("/api/formbootstrapper")
                .subscribe(response => {
                        this.configInfo = response;
                        observer.next(response);
                        observer.complete();
                    },
                err => {
                    observer.err();
                        //alert("Couldn't load bootstrapper info");
                    });


        });

    }
}
