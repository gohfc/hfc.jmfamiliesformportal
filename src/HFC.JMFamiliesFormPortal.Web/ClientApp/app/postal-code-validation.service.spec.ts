import { TestBed } from '@angular/core/testing';

import { PostalCodeValidationService } from './postal-code-validation.service';

describe('PostalCodeValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostalCodeValidationService = TestBed.get(PostalCodeValidationService);
    expect(service).toBeTruthy();
  });
});
